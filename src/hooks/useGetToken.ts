import { useState, useEffect } from "react";
import {setLocalState} from '@utils/localStorage';

export const useGetToken = () => {
    const [tvMovieAuthentication, setToken ] = useState('')
    const [loadingToken, setLoading ] = useState(true)
    const [error, setError ] = useState('')

    let url = `${process.env.NEXT_PUBLIC_API_URL}authentication/token/new?api_key=${process.env.NEXT_PUBLIC_API_KEY}`
    
    useEffect(() => {
        async function getToken() {
          const response = fetch(`${url}`)
          .then(response => response.json())
          .then(response => response)
          .catch(errors => setError(errors));

          const tvToken = await response;
          if(tvToken?.success){
            setLocalState('datiatv_access_token', tvToken.request_token);
            setLocalState("datiatv_expires_at", tvToken.expires_at);
            setToken(tvToken.request_token);
          }
          
          setLoading(false);
        }
        // TODO: Validate the Expire date Token 
        getToken();
    },[])
    return [tvMovieAuthentication, loadingToken, error]
}