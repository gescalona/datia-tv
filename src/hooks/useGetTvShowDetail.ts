import { useState, useMemo } from "react";

type DetailsProps ={
  tvShowId: any
}
export const useGetTvShowDetail = ({ tvShowId }: DetailsProps) => {
    const [details, setDetails ] = useState([])
    const [loading, setLoading ] = useState(true)
    const [error, setError ] = useState('')

    let url = `${process.env.NEXT_PUBLIC_API_URL}tv/${tvShowId}?api_key=${process.env.NEXT_PUBLIC_API_KEY}&language=${process.env.NEXT_PUBLIC_LANGUAJE}`
    
    useMemo(() => {
        async function getDetail() {
          const response = fetch(`${url}`)
          .then(response => response.json())
          .then(response => response)
          .catch(errors => setError(errors));

          const details = await response;
          // console.log(details)
          setDetails(details);
          setLoading(false);
        }
        getDetail();
    },[tvShowId])
    return [details, loading, error]
}
