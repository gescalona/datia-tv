
import { useState, useMemo } from "react";

type GetSessions = {
    session_id: any;
    review_value: any;
}
export const useRatingTvShow = ({session_id = null, review_value}: GetSessions ) => {
    const [tvReviewSuccess, setSuccess ] = useState(false);
    const [tvReviewMessage, setMessage ] = useState('')
    const [tvReviewLoading, setLoading ] = useState(true)
    const [makeRatingRequest, setRatingRequest ] = useState(false)
    const [error, setError ] = useState('')

    let url = `${process.env.NEXT_PUBLIC_API_URL}tv/4.3/rating?api_key=${process.env.NEXT_PUBLIC_API_KEY}&session_id=${session_id}`
    //console.log(review_value, " THE URL TO REVIEW VALU ");
    
    useMemo(() => {
        async function sendReview() {
          const response = fetch(`${url}`,{
            method: 'POST',
            headers:{
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({"value": review_value})
        })
        .then(response => response.json())
        .then(response => response)
        .catch(errors => { console.log("ERROR: ", errors);  setError(errors)});

            const tvToken = await response;
            console.log(tvToken, " Review response");
            
            if(tvToken?.success){
                setSuccess(tvToken.request_token);
                setMessage(tvToken.status_message)
                setRatingRequest(true);
            }
            setLoading(false);
        }
        if(session_id !== undefined && session_id !== null){
            sendReview();
        }
        
    },[session_id])
    return [tvReviewSuccess, tvReviewLoading, error, makeRatingRequest, tvReviewMessage]
}