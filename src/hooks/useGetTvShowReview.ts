import {useState, useMemo } from 'react';

type SearchProps ={
  tvShowId: any,
  page?: any
}
export const useGetTvShowReview = ({ tvShowId, page }: SearchProps) => {
  const [tvShowReviews, setTvReviews] = useState({results: []});
  const [errors, setError] = useState('');
  const [loading, setLoading] = useState(true);

  let url = `${process.env.NEXT_PUBLIC_API_URL}tv/${tvShowId}/reviews?api_key=${process.env.NEXT_PUBLIC_API_KEY}&language=${process.env.NEXT_PUBLIC_LANGUAJE}&page=${page}`
  
  useMemo(() => {
    async function getTvReviews() {
      const response = fetch(`${url}`)
        .then(response => response.json())
        .then(response => response)
        .catch(errors => setError(errors));

        const tvReview = await response;
        
        setTvReviews(tvReview)
        setLoading(false)
    }
    getTvReviews()
  }, [tvShowId, page])

  return {tvShowReviews, loading, errors};

}

