import {useState, useMemo } from 'react';

type SearchProps ={
  searchText: String,
  page?: any
}
export const useSearchTvShow = ({ searchText, page }: SearchProps) => {
  const [tvShows, setTvShows] = useState({results: []});
  const [errors, setError] = useState('');
  const [loading, setLoading] = useState(true);

  if (searchText.length <=0 ) searchText = "The";
  let url = `${process.env.NEXT_PUBLIC_API_URL}search/tv?api_key=${process.env.NEXT_PUBLIC_API_KEY}&language=${process.env.NEXT_PUBLIC_LANGUAJE}&page=${page}&query=${searchText}&include_adult=false`
  
  useMemo(() => {
    async function getTvShows() {
      const response = fetch(`${url}`)
        .then(response => response.json())
        .then(response => response)
        .catch(errors => setError(errors));
        const tvShows = await response;
        
        setTvShows(tvShows)
        setLoading(false)
    }
    if(searchText?.length > 2){
      getTvShows()

    }
  }, [searchText, page])

  return {tvShows, loading, errors};

}

