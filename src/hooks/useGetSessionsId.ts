import { useState, useContext, useMemo } from "react";
import { setLocalState} from '@utils/localStorage';
import {AuthContext} from '@datiatv/context/auth/auth.context';

type GetSessions = {
    request_token: any;
    approved?: any;
}
export const useGetSessionsId = ({request_token}: GetSessions ) => {
    const [tvMovieSessionId, setSessionId ] = useState(null)
    const [loading, setLoading ] = useState(true)
    const [error, setError ] = useState('')
    const {authDispatch} = useContext<any>(AuthContext);
    
    let url = `${process.env.NEXT_PUBLIC_API_URL}authentication/session/new?api_key=${process.env.NEXT_PUBLIC_API_KEY}`
    
    useMemo(() => {
        async function getToken() {
          const response = fetch(`${url}`,{
            method: 'POST',
            headers:{
              'Content-Type': 'application/json'
             },
            body: JSON.stringify({"request_token": request_token})
        })
          .then(response => response.json())
          .then(response => response)
          .catch(errors => { console.log(errors, " ERROR USE GET SESSION");  setError(errors)});

          const tvToken = await response;
          if(tvToken?.success){

            setLocalState('datiatv_session_id', tvToken.session_id);
            setLocalState('datiatv_session_expires_at', tvToken)
            setSessionId(tvToken.session_id);
            authDispatch({ type: 'SIGNIN_SUCCESS' })
          }
          
          setLoading(false);
        }
        // TODO: Validate the Expire date Token 
        getToken()
        
    },[request_token])
    return [tvMovieSessionId, loading, error]
}