import React from 'react';
import Logo from './logo/logo';
import {
  DataiContainer,
  DataiHeader,
  DatiaContent,
  DatiaFooter
} from './styled';

type LayoutProps = {
    children?: React.ReactNode;
  };

const Layout: React.FunctionComponent<LayoutProps> = ({
    children
  }) => {
    return (
      <DataiContainer>
        <DataiHeader>
          <Logo alt="Datia | TV" imageUrl="https://datia.app/dist/images/logo.svg" />
        </DataiHeader>
        <DatiaContent>
          {children}
        </DatiaContent>
            
        <DatiaFooter>
        </DatiaFooter>      
      </DataiContainer>
    )
}

export default Layout;