import styled from 'styled-components';

export const DataiContainer = styled.main`
    display: flex;
    flex-direction: column;
    align-items:center;
    margin:0;
    padding: 0;
    width: 100%;
    max-width: 1440px;
`;

export const DataiHeader = styled.header`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    height: 60px;
    background-color: #0B0D19;
    color: #FFF;
`;

export const DatiaContent = styled.section`
    display: flex;
    justify-content: center;
    align-items: initial;
    position: relative;
    height: auto;;
    width: 100%;
    max-width: 1024px;
`;

export const DatiaFooter = styled.footer`
    display: flex;
    bottom: 0;
    width: 100%;
    height: 80px;
    background-color:#5A1CFE;
    color: #FFF;
`;