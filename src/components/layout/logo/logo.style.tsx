import styled from "styled-components";

export const LogoBox = styled.span`
    display: flex;
    align-items: center;
    width: 100px;
    @media (max-width: 990px) {
        justify-content: center;
        width: 80px;
    }
`;

export const LogoImage = styled.img`
    display: block;
    backface-visibility: hidden;
    width: 60%;
    margin: 5px 10px;
    cursor: pointer;
`;
