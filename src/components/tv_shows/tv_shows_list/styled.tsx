import styled from 'styled-components';

export const TvListContainer = styled.section`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    max-width: 1024px;
    list-style: none;
    height: auto;
    background-color: #d7dcf7;
`;

export const TvListContent = styled.ul`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    justify-content: center;
    align-items: center;
    width: 100%;
    max-width: 1024px;
    list-style: none;
    height: auto;
`;

export const SearchContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 30px 0 ;
    width: 100%;
    height: auto;
    background-color: #7A43DB;
`
export const SearchText = styled.div`
    color: #FFF;
    h2{
      font-size: 2em;
      font-weight: 700;
      line-height: 0.2;
    }
    h3 {
      font-size: 1em;
      font-weight: 600;
      margin: 0 0 17px 0;
    }
`;
export const SearchInput = styled.input`
    width: 400px;
    height: 20px;;
    border-radius: 20px;
    border: 0;
    padding: 10px;
    color: #7a43db;
    &::placeholder{
        color: #b0a3c9;
    }
`