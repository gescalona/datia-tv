import React from 'react';
import TvShowsCard from '../tv_shows_card'
import {useSearchTvShow} from '@datiatv/hooks/useSearchTvShow';
import {
  TvListContent,
  TvListContainer,
  SearchContainer,
  SearchInput,
  SearchText
} from './styled';

const TvShowList: React.FC  = () => {
    const [searchVal, setSearchVal] = React.useState('');
    const [finalVal, setFinalVal] = React.useState('');

    const { tvShows, loading } = useSearchTvShow( { searchText: finalVal, page : 1} );
    
    const handleChange = (event: any) => {
      setSearchVal(event.target.value);
    };

    const handleKeyDown = (event: any) => {
      if (event.key === 'Enter') {
        setFinalVal(searchVal)
      }
    }
  
    
    return (
      <TvListContainer>
        <SearchContainer>
          <SearchText>
            <h2>Welcome.</h2>
            <h3>Millions of movies, TV shows and people to discover. Explore now.</h3>
          </SearchText>
          <SearchInput 
            value={searchVal}
            onChange={handleChange}
            onKeyDown={handleKeyDown}
            // onKeyUp={handleChange}
            placeholder="Search your favorite tv Show"  />
        </SearchContainer>
      <TvListContent>
      { !loading && 
        tvShows?.results?.map((tv : any) => 
          {
            return  <TvShowsCard key={tv.id} tvObj={tv} />
          }
        )
      } 

      </TvListContent>
      </TvListContainer>
    )
}

export default TvShowList;
