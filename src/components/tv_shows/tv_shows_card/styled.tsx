import styled from 'styled-components';

export const TvCard = styled.li`
    background: rgb(255, 255, 255);
    width: 208px;
    min-width: auto;
    padding: 8px 8px 14px;
    height: auto;
    border-radius: 12px;
    margin: 4px;
    transition: box-shadow 0.2s ease-in-out 0s;
    overflow: hidden;
    box-sizing: border-box;
    cursor: pointer;
    &:hover {
        box-shadow: 0 0 11px #CCCCCC;
    }
    h1 {
        font-size: 14px;
        font-weight: bolder;
    }
    h3 {
        font-size: 12px;
        font-weight: lighter;
    }
`;

export const TvCardLink = styled.a`
    color: #000;
    text-decoration: none;
`;
export const TvCardImageWrapper = styled.div`
  margin-bottom: 8px;
  border-radius: 8px;
  overflow: hidden;
  position: relative;
  height: 192px;
  display: flex;
  justify-content: center;
  @media (max-width: 469px) {
    height: 140px;
  }
`;

export const TvCardImage = styled.img`
  object-fit: unset;
  width: 100%;
`;

export const TvAvrageWrapper =styled.span`
    width: 100%;
`;

interface AverageProps {
    average?: any
}
export const TvAverage = styled.span<AverageProps>`
    width: ${props => props.average}%;
    border-radius: 20px;
    padding: 0px 5px;
    color: #FFF;
    font-size: 11px;
    text-align: center;
    background: linear-gradient(to right, rgba(249, 215, 61, 1) 0%, rgba(41, 198, 67, 1) 100%);
    margin-top: -6px;
    height: 10px;
    margin-bottom: 5px;
    justify-content: center;
    align-items: center;
    display: flex;
`;
