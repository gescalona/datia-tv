import React from 'react';
import { 
  TvCard,
  TvCardLink,
  TvCardImageWrapper,
  TvAvrageWrapper,
  TvAverage,
  TvCardImage
} from './styled';

type TvShowsCardProps = {
    tvObj: any;
}

const TvShowsCard: React.FC<TvShowsCardProps>  = ({
  tvObj
}) => {
  
  let title  = tvObj.original_name;
  let tvDate =  new Date(tvObj.first_air_date);
  return (
    <TvCard key={tvObj.id}>
      <TvCardLink href={`/details/${tvObj.id}`}>
        <TvCardImageWrapper>
          <TvCardImage src={`//image.tmdb.org/t/p/w94_and_h141_bestv2/${tvObj.poster_path}`} loading="lazy" />
        </TvCardImageWrapper>
        <TvAvrageWrapper>
          <TvAverage average={(parseFloat(tvObj.vote_average) * 10)}>
            {(parseFloat(tvObj.vote_average) * 10)}%
          </TvAverage>
        </TvAvrageWrapper>

          <h1>{title}</h1>
          <h3>{tvObj.media_type}</h3>
          <h3>{tvDate.toLocaleDateString()}</h3>
      </TvCardLink>
    </TvCard>
  )
  }

  export default TvShowsCard;