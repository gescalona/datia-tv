import styled from 'styled-components';



export const TvShowsDetailsContainer = styled.section`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: auto;
`;


interface DetailsWrapperProps {
  bckImage?: any
}

export const TvShowsDetailsWrapper = styled.section<DetailsWrapperProps>`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100vh;
    background-image: url(${props => props.bckImage});
    background-repeat: no-repeat;
    background-position: right 0px top;
    background-size: cover;
    span {
        background-image: linear-gradient(to right, rgba(11, 13, 25, 0.8) 150px, rgba(90, 28, 254, 0.84) 100%);
        width: inherit;
        height: inherit;
    }
`;

export const TvShowsDetailsContent = styled.article`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  color:#FFF;
  width: 98%;
  height: 100vh;
`;

export const TvShowsDetailsLeft = styled.article`
    width: 35%;
`;

export const TvShowsDetailsRight = styled.article`
  width: 65%;
`;
export const TvShowsDetailsImgWrapper =  styled.article`
  
  display: block;
  min-width: 300px;
  width: 300px;
  height: 450px;
  position: relative;
  top: 0;
  left: 0;
  
`;
export const TvShowsDetailsMainImg =  styled.img`
display: block;
    width: 100%;
    min-width: 100%;
    height: 100%;
    min-height: 100%;
    border-width: 0px;
  outline: none;
  border-radius: 10px;
`;

export const TvShowsDetailsInfo = styled.article`
  display: flex;
  justify-content: flex-start;  
  flex-direction: column;
`;

export const TvShowsDetailsHead = styled.div`
    display: grid;
    justify-content: center;
    align-items: center;
    grid-template-columns: 60% 40%;
    width: 100%;
    height: auto;
    
    li {
      padding-right: 4px;
      font-size: 12px;
      &:after {
        content: ","
      }
      &:last-child:after {
        content: "."
      }
    }
`;

export const DetailList = styled.ul`
  display: flex;
  list-style: none;
  padding: 0;
`;
export const Title = styled.div`
  font-size:30px;
`;
export const Date = styled.div`
  font-size:18px;
  font-weight: lighter;
`;

export const OverView = styled.p`
  font-size: 12px;
`;

export const CompanyName = styled.h3`
  font-size: 12px;
  font-weight: lighter;
`;


export const CompanyImage = styled.img`
  width: 50px;
  height: 50px;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const TvShowAditionalInfo = styled.section`
  display: grid;
  width: 96%;
  justify-content: center;
  padding: 20px;
  grid-template-columns: 50% 50%;

`;