import React from 'react'
import styled from 'styled-components';
import { useGetTvShowReview } from '@datiatv/hooks/useGetTvShowReview';


const ReviewContainer = styled.main`
    display: flex;
    width: 100%;
    height: auto;
    padding: 10px;
    ul{
        padding: 0;
        list-style: none;
    }
    li {
      font-size: 14px;
      font-weight: lighter;
      padding: 4px;
      border-bottom: 1px solid #CCC;
      h5 {
          font-size: 18px;
      }
    }
`;

type TvShowReviewProps = {
  tvObj: any  
}

const TvShowReview: React.FC<TvShowReviewProps> = ({
  tvObj
}) => {

  const  {tvShowReviews, loading } = useGetTvShowReview( { tvShowId: tvObj.id, page: 1} );
    return (
      <div>
        <h1>Reviews</h1>
      <ReviewContainer>
        <ul>
          {
            !loading &&
            tvShowReviews?.results?.map((review: any) => (
              <li key={review.id}>
                  <h5>A review by {review.author}</h5>
                  {review.content}
              </li>
            ))
          }
        </ul>
      </ReviewContainer>
      </div>
    )
}

export default TvShowReview;
