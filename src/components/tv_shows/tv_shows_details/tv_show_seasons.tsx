import React from 'react'
import {
    TvShowsDetailsHead,
    CompanyImage,
    CompanyName
} from './styled';

type TvShowSeasonsProps = {
    tvObj: any  
}
const TvShowSeasons: React.FC<TvShowSeasonsProps> = ({
    tvObj
  }) => {


  return (
    <div>
      <h1>Seasons</h1>
      { tvObj?.seasons?.map((season : any) => (
        <TvShowsDetailsHead key={season.id}>
          <CompanyName>
            {season.name}
          </CompanyName>
          <CompanyImage src={`https://image.tmdb.org/t/p/w300_and_h450_bestv2/${season.poster_path}`} />
        </TvShowsDetailsHead>
        )) 
    }
    </div>
    )
}

export default TvShowSeasons
