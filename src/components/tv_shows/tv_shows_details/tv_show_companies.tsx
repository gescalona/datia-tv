import React from 'react'
import {
    TvShowsDetailsHead,
    CompanyImage,
    CompanyName
} from './styled';

type TvShowCompaniesProps = {
    tvObj: any  
}
const TvShowCompanies: React.FC<TvShowCompaniesProps> = ({
    tvObj
  }) => {
// console.log(tvObj);

  return (
    <div>
       <h1>Production companies</h1>
      { tvObj?.production_companies?.map((company : any) => (
        <TvShowsDetailsHead key={company.id}>
          <CompanyName>
            {company.name}
          </CompanyName>
          <CompanyImage src={`https://image.tmdb.org/t/p/w300_and_h450_bestv2/${company.logo_path}`} />
        </TvShowsDetailsHead>
        )) 
    }
    </div>
    )
}

export default TvShowCompanies
