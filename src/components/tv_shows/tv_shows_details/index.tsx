import React from 'react';
import { useRouter } from 'next/router';
import ReactStars from 'react-rating-stars-component';
import { useGetToken } from '@datiatv/hooks/useGetToken';
import {useGetSessionsId} from '@datiatv/hooks/useGetSessionsId';
import {useRatingTvShow} from '@datiatv/hooks/useRatingTvShow';

import {SEO} from '@datiatv/components/seo';
import TvShowReview from './tv_show_review';
import TvShowCompanies from './tv_show_companies';
import TvShowSeasons from './tv_show_seasons';
import {
    TvShowsDetailsWrapper,
    TvShowsDetailsContainer,
    TvShowsDetailsContent,
    TvShowsDetailsLeft, 
    TvShowsDetailsRight,
    TvShowsDetailsImgWrapper,
    TvShowsDetailsMainImg,
    TvShowsDetailsInfo,
    TvShowsDetailsHead,
    TvShowAditionalInfo,
    Title,
    Date,
    DetailList,
    OverView,
} from './styled';

type TvShowsDetailsProps ={
    tvObj: any;
    children?: React.ReactNode;
}
const TvShowsDetailsComponent: React.FC<TvShowsDetailsProps>  = ({
  tvObj
}) => {
  const router = useRouter();
  const { request_token, ratings } = router.query;
  // console.log(" OUTSIDE EFFECT => ", request_token, approved, ratings, " <=== OUTSIDE EFFECT");
  const [tvMovieAuthentication] = useGetToken()
  const [tvMovieSessionId] = useGetSessionsId({request_token: request_token})

  const [tvReviewSuccess, tvReviewLoading, tvReviewMessage] = useRatingTvShow({ session_id: tvMovieSessionId, review_value: ratings });
  // const { authState: { isAuthenticated } } = React.useContext<any>(AuthContext);

  const ratingChanged = (newRating: any) => {
    loginCallback(newRating)
  };
  
  const loginCallback = (newRating: any) => {
    if (process.browser && typeof window !== 'undefined' && tvMovieAuthentication !== undefined ) {
      let redirect = `${window.location.origin}${window.location.pathname}?ratings=${newRating}`;
      router.push(`https://www.themoviedb.org/authenticate/${tvMovieAuthentication}?redirect_to=${redirect}`);
      return;
    }
  };


    return (
      <TvShowsDetailsContainer>
        <TvShowsDetailsWrapper bckImage={`//image.tmdb.org/t/p/w300_and_h450_bestv2/${tvObj?.backdrop_path}`}>
          <span>
            <SEO title={`Datia Tv - ${tvObj?.original_name}`}
                 description={`Datia - ${tvObj?.overview}` }
            />
            { !tvReviewLoading && tvReviewSuccess && (<h1>{tvReviewMessage} NADA </h1>) }
            <TvShowsDetailsContent>
              
              <TvShowsDetailsLeft>
                <TvShowsDetailsImgWrapper >
                  <TvShowsDetailsMainImg src={`https://image.tmdb.org/t/p/w300_and_h450_bestv2/${tvObj?.poster_path}`}/>
                </TvShowsDetailsImgWrapper>
                
                { tvObj?.vote_average !== undefined && <ReactStars 
                            count={5}
                            onChange={ratingChanged}
                            isHalf={true}
                            value={parseFloat(tvObj.vote_average) / 2}
                            size={24}
                            activeColor="#ffd700"
                          />
                }
              </TvShowsDetailsLeft>
              <TvShowsDetailsRight>
                <TvShowsDetailsInfo>
                   <TvShowsDetailsHead>
                      <Title>{tvObj?.original_name}</Title>
                      <Date>({tvObj?.first_air_date})</Date>
                   </TvShowsDetailsHead>
                   <TvShowsDetailsHead>
                     <DetailList>
                       { tvObj?.genres?.map((genero: any ) => ( 
                          <li key={genero.id}>{genero.name}</li>
                         ))
                       }
                    </DetailList>
                   </TvShowsDetailsHead>

                    <Date>Overview</Date>
                    <OverView>{tvObj.overview}</OverView>
                </TvShowsDetailsInfo>
              </TvShowsDetailsRight>
              
            </TvShowsDetailsContent>
          </span>
        </TvShowsDetailsWrapper>
        
        <TvShowAditionalInfo>
          <TvShowCompanies tvObj={tvObj} />

          <TvShowSeasons tvObj={tvObj} />
        </TvShowAditionalInfo>
        

        <TvShowReview tvObj={tvObj}/>
      </TvShowsDetailsContainer>
    )
}

export default TvShowsDetailsComponent;
