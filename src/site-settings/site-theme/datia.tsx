const baseColor = {
  primaryDark: '#FA651B',
  primaryPurple: '#30C3BC',
  serviGreen: "#24913D",
  arawatoBlue: '#32CAC3',
  primaryLight: '#FEFFFF',
  primaryWhite: '#FEFFFF',
  primaryGray: '#FAFAFA',
  primaryYellow: '#F9D73D',
  primaryHover: '#ffd3bf',
  mobile: '576px',
  info: '#232525' /* Shark */,
  success: '#5e9850' /* Fruit Salad */,
  warning: '#db8818' /* Zest */,
  danger: '#f44336' /* Pomegranate */,
  white: "#ffffff",
  black: "#000000",
  gray: {
    100: "#f9f9f9",
    200: "#F7F7F7",
    300: "#f4f4f4",
    400: "#F3F3F3",
    500: "#f1f1f1", // border alt color
    600: "#EdEdEd",
    700: "#E6E6E6", // border color
    800: "#C2C3CC",
    900: "#bdbdbd"
  },
    text: {
        bold: "#0D1136", // heading color
        medium: "#424561",
        regular: "#77798C", // regular text color
        light: "#909090",
        label: "#767676"
    },
    transparent: "transparent",
    primary: {
        regular: "#83249a", // primary color
        hover: "#6e0189",
        alternate: "#680282",
        light: "#dbc5e0"
    },
    secondary: {
        regular: "#ff5b60",
        hover: "#FF282F",
        alternate: "#fc5c63"
    },
    yellow: {
        regular: "#F9D73D", //'#FFAD5E',
        hover: "#F4DA66", //'#FFB369',
        alternate: "#EACB3F" //'#f4c243',
    },
    blue: {
        regular: "#2e70fa",
        dark: "#161F6A",
        light: "#666D92",
        link: "#4285f4"
    },
    red: "#ea4d4a",
    muted: "",
    highlight: ""
};

const palette = {
    principalColor: "#EF6431",
    greenColor: "#004A0B",
    secondaryColor: "rgba(239, 100, 49,0.4)",
    commonColor: "#fff",
    hoverButonsIcon: "#d5d8dc",
    borderColor: "#e6e6e6",
    colorButton: "#EF6431",
    colorDelete: "#e13507",
    colorButtonCart: "linear-gradient(118deg, rgb(116 249 15), rgb(0 114 255))",
    spanCartColor: " #424242",
    cardColorP: "rgba(59, 57, 53, 0.7)",
    shadow: "rgba(33, 33, 33, 0.2)",
    errorColor: "red",
    bgTextFields: "rgba(230, 229, 227, 0.3)",
    bgTextFieldsError: "rgba(255, 23, 0, 0.1)",
    bgList: "#EEEEEE",
  };
const paletteHome = {
    principalColor: "#ff6700",
    bgButton: "#ff7340",
    greeColor: "#004A0B",
    letterColor: "#2f3c4d",
    bgHeaderHome: "rgba(173,168,168,0.1)",
    bigTitleColor: "#b7b7b7",
    footerColor: "rgba(0,74,11)",
    shadow: "rgba(33, 33, 33, 0.2)",
  };
export const overlayColor = (opacity: any) => {
    return `rgba(0, 0, 0, ${opacity})`;
  };

export const defaultTheme = {
    palette: {
      ...paletteHome,
      ...palette
    },
    colors: {
        ...baseColor,
        body: {
            bg: "",
            text: ""
        },
        borderColor: "gray.500",
        headingsColor: "text.bold",
        subheadingsColor: "",
        textColor: "text.regular",
        buttonColor: "white",
        buttonBgColor: "primary.regular",
        buttonBgHoverColor: "primary.hover",
        buttonBorderColor: "primary.regular",
        linkColor: "primary.regular",
        input: {
            text: "",
            bg: "",
            border: "",
            focus: "",
            placeholder: ""
        }
    },
    breakpoints: ["767px", "991px", "70em", "90em"],
    
    space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
    fontSizes: {
        xs: 12,
        sm: 13,
        base: 15,
        md: 19,
        lg: 21,
        xl: 24,
        "2xl": 30,
        "3xl": 36,
        "4xl": 42,
        "5xl": 48
    },
    fontWeights: {
        body: 400,
        heading: 700,
        thin: 100,
        light: 300,
        regular: 400,
        medium: 500,
        semiBold: 600,
        bold: 700,
        bolder: 900
    },
    fonts: {
        body: "Lato, sans-serif",
        heading: "Poppins, sans-serif",
        monospace: "Menlo, monospace"
    },
    customs: {
        transition: "0.35s ease"
    },
    lineHeights: {
        body: 1.5,
        heading: 1.125
    },
    boxSizing: "border-box",
    radii: {
        base: "6px",
        small: "3px",
        medium: "12px",
        big: "18px"
    },
    shadows: {
        base: "0 3px 6px rgba(0, 0, 0, 0.16)",
        medium: "0 6px 12px rgba(0, 0, 0, 0.16)",
        big: "0 21px 36px rgba(0, 0, 0, 0.16)",
        header: "0 1px 2px rgba(0, 0, 0, 0.06)"
    }
};