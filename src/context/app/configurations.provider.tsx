import React, {useState, useEffect, useContext} from 'react';
import { ConfigContext } from './configurations.context';

// type ConfProps = {
//     children: React.ReactNode;
// }
export const ConfigurationProvider: React.FunctionComponent = ({ children }) => {

    const [tvMovieConfigurtion, setTvMovieConfigurtion] = useState({base_url: String});


    let url = `${process.env.NEXT_PUBLIC_API_URL}configuration?api_key=${process.env.NEXT_PUBLIC_API_KEY}`
    // https://api.themoviedb.org/3/configuration?api_key=3504a963b5eddb74923319a7e1dab880
    useEffect(() => {
        async function getConfigurations() {
            const response = window.fetch(`${url}`)
              .then(response => response.json())
              .then(response => response)
              .catch(errors => console.log(errors));
              const tvMovieConf = await response;
              // console.log(tvMovieConf, "useEffect");
              setTvMovieConfigurtion(tvMovieConf)
              
          }
        getConfigurations()
    }, [])

    function getUrlImages() {
      return tvMovieConfigurtion?.base_url;
    } 


    const objValue = React.useMemo(() => {
      return ({
        tvMovieConfigurtion,
        getUrlImages
      })
    }, [])

    return (
        <ConfigContext.Provider value={objValue}>
            {children}
        </ConfigContext.Provider>
      );
};

export const useConfigurations = () => {
  const context = useContext(ConfigContext);
  if(!context){
      throw new Error("Context Error ");
  }
  return context;
}