export const REVIEW_DATA = {
  "id": 76479,
  "page": 1,
  "results": [
    {
      "author": "YouShouldKnow",
      "content": "Wonderful premise, beautiful pictures, handsome, good and sexy actors, what could go wrong?\r\n\r\nUnnecessary, stupid decisions.\r\nEverywhere.\r\n\r\nThe typical \"say something!\" \\*stuttering and stammering* to introduce some side-story, or secret personas where the only thing to change is the hero's suit, or a wimpy protagonist who has way too many scenes of indecisiveness, long and drawn out dialogues (seriously 2x the speed and it's like things are normal again), extraneous lies to create drama, etc. etc. etc.\r\n\r\nIt's like a trope collection turned into a superhero series with a \"*gotcha!*\" twist. Seriously, the acting and the story is good, but the tropes kill every redeeming quality.\r\n\r\nIt's a tragedy",
      "id": "5d4b400000213400122610b1",
      "url": "https://www.themoviedb.org/review/5d4b400000213400122610b1"
    },
    {
      "author": "Lenny_H",
      "content": "A great series set in a world where superheroes are a real phenomenon.\r\n\r\nCapitalism is omnipresent, profit and money are in everyone's mind.\r\nOf course the 'Sups' abuse their power for their own interests and many more cruel and immoral things.\r\nEvery hero's doing is explained, some more, some less. There's a perfect balance between big serious storylines and little kind of darker humor ones. \r\n\r\n'The Boys' are trying to destroy this whole system due to personal problems with some of the 'Sups'. The focus clearly is around them and what they're doing. \r\nSo it's definitely an Anti-Superhero show which look and topic reminds me of 'Watchmen' or some other more Noir candidates in this genre.\r\n\r\nThe whole series is explicit, which adds up really well. Without the violence and the handling with sexual situations the whole show wouldn't be that impressive and near to the reality. Not appropriate for younger viewers, if you ask me.",
      "id": "5d56a5124bc38b3503266cfa",
      "url": "https://www.themoviedb.org/review/5d56a5124bc38b3503266cfa"
    },
    {
      "author": "josalarcon2",
      "content": "For me the whole series is a total and effective satire that lets us see what would become of the world if fiction becomes reality: heroes lose interest in saving the world (or take advantage of its status), are forced to do things that are not right, to follow the company's own interests and do not go out to end the crime if they are not followed by a camera that captures each of their movements, even though they are always transmitting that image of exemplary citizen when in fact they are greedy and treat their fans badly.\r\n\r\nIt's also chilling to know that people in power act as judges and executioners to decide what crimes are profitable and what millions of dollars they rent their superheroes to other cities so they can be safe. The precision in the denunciation of The Boys is one of its strongest points since satire towards these characters is not new, but combining it with the theme of corporate America and wild capitalism gives it a clear differential touch.\r\n\r\nThe series is a total hit and is perfectly developed, to the point of making it impossible to miss because chapter by chapter the anguish increases. So if you have free time, I strongly recommend you to watch this satirical series.",
      "id": "5d5cc8f7a3d02700140b4c3b",
      "url": "https://www.themoviedb.org/review/5d5cc8f7a3d02700140b4c3b"
    }
  ],
  "total_pages": 1,
  "total_results": 3
}

export const DETAIL_DATA = {
  "backdrop_path": "/bI37vIHSH7o4IVkq37P8cfxQGMx.jpg",
  "created_by": [
    {
      "id": 58321,
      "credit_id": "5f3814c011c066003637f6a8",
      "name": "Eric Kripke",
      "gender": 2,
      "profile_path": "/dd7EgfOEKPqQxWtBfAvjYZahbSc.jpg"
    }
  ],
  "episode_run_time": [
    60
  ],
  "first_air_date": "2019-07-25",
  "genres": [
    {
      "id": 10765,
      "name": "Sci-Fi & Fantasy"
    },
    {
      "id": 10759,
      "name": "Action & Adventure"
    }
  ],
  "homepage": "https://www.amazon.com/dp/B0875L45GK",
  "id": 76479,
  "in_production": true,
  "languages": [
    "en"
  ],
  "last_air_date": "2020-10-02",
  "last_episode_to_air": {
    "air_date": "2020-10-02",
    "episode_number": 7,
    "id": 2324563,
    "name": "Butcher, Baker, Candlestick Maker",
    "overview": "Congresswoman Victoria Neuman’s sham Congressional Hearing against Vought takes place in 3 DAYS. Are we going to let her criminalize Superheroes when we need them most? We have to stand up against such blatant partisan politics. Please join fellow Patriotic Americans and send $20 to VOUGHTPROMISE.COM to tell Neuman and her Kangaroo Court Cronies that they won’t win, “Not On Our Watch”™.",
    "production_code": "THBY 207",
    "season_number": 2,
    "show_id": 76479,
    "still_path": "/8OJpTYvdGaTkzxU9oZnx0TaTaOt.jpg",
    "vote_average": 7.333,
    "vote_count": 3
  },
  "name": "The Boys",
  "next_episode_to_air": {
    "air_date": "2020-10-09",
    "episode_number": 8,
    "id": 2324567,
    "name": "What I Know",
    "overview": "",
    "production_code": "",
    "season_number": 2,
    "show_id": 76479,
    "still_path": null,
    "vote_average": 0,
    "vote_count": 0
  },
  "networks": [
    {
      "name": "Amazon",
      "id": 1024,
      "logo_path": "/ifhbNuuVnlwYy5oXA5VIb2YR8AZ.png",
      "origin_country": "US"
    }
  ],
  "number_of_episodes": 16,
  "number_of_seasons": 2,
  "origin_country": [
    "US"
  ],
  "original_language": "en",
  "original_name": "The Boys",
  "overview": "A group of vigilantes known informally as “The Boys” set out to take down corrupt superheroes with no more than blue-collar grit and a willingness to fight dirty.",
  "popularity": 1699.141,
  "poster_path": "/mY7SeH4HFFxW1hiI6cWuwCRKptN.jpg",
  "production_companies": [
    {
      "id": 20580,
      "logo_path": "/tkFE81jJIqiFYPP8Tho57MXRQEx.png",
      "name": "Amazon Studios",
      "origin_country": "US"
    },
    {
      "id": 333,
      "logo_path": "/5xUJfzPZ8jWJUDzYtIeuPO4qPIa.png",
      "name": "Original Film",
      "origin_country": "US"
    },
    {
      "id": 11073,
      "logo_path": "/wHs44fktdoj6c378ZbSWfzKsM2Z.png",
      "name": "Sony Pictures Television",
      "origin_country": "US"
    },
    {
      "id": 38398,
      "logo_path": null,
      "name": "Kripke Enterprises",
      "origin_country": "US"
    },
    {
      "id": 16615,
      "logo_path": "/dbxHaOtibTKvP6dUMRYdwUOULgY.png",
      "name": "Point Grey Pictures",
      "origin_country": "US"
    },
    {
      "id": 2526,
      "logo_path": "/kzIW6QWhacDwzGoXEIWhac8rb70.png",
      "name": "Kickstart",
      "origin_country": "US"
    },
    {
      "id": 33728,
      "logo_path": null,
      "name": "NightSky Productions",
      "origin_country": "US"
    }
  ],
  "seasons": [
    {
      "air_date": "2020-09-09",
      "episode_count": 1,
      "id": 163277,
      "name": "Specials",
      "overview": "",
      "poster_path": "/cCXG81dSCPXcqYm6gTHlwtXocti.jpg",
      "season_number": 0
    },
    {
      "air_date": "2019-07-25",
      "episode_count": 8,
      "id": 98085,
      "name": "Season 1",
      "overview": "",
      "poster_path": "/g3HjUeFCwKOfBxlM97lv016mnol.jpg",
      "season_number": 1
    },
    {
      "air_date": "2020-09-04",
      "episode_count": 8,
      "id": 154681,
      "name": "Season 2",
      "overview": "The even more intense, more insane season two finds The Boys on the run from the law, hunted by the Supes, and desperately trying to regroup and fight back against Vought. In hiding, Hughie, Mother’s Milk, Frenchie and Kimiko try to adjust to a new normal, with Butcher nowhere to be found. Meanwhile, Starlight must navigate her place in The Seven as Homelander sets his sights on taking complete control. His power is threatened with the addition of Stormfront, a social media-savvy new Supe, who has an agenda of her own. On top of that, the Supervillain threat takes center stage and makes waves as Vought seeks to capitalize on the nation’s paranoia.",
      "poster_path": "/mY7SeH4HFFxW1hiI6cWuwCRKptN.jpg",
      "season_number": 2
    }
  ],
  "status": "Returning Series",
  "type": "Scripted",
  "vote_average": 8.4,
  "vote_count": 2580
}

export const TEST_DATA = {
    "page": 1,
    "results": [
      {
        "id": 497582,
        "video": false,
        "vote_count": 1523,
        "vote_average": 7.7,
        "title": "Enola Holmes",
        "release_date": "2020-09-23",
        "original_language": "en",
        "original_title": "Enola Holmes",
        "genre_ids": [
          80,
          18,
          9648
        ],
        "backdrop_path": "/kMe4TKMDNXTKptQPAdOF0oZHq3V.jpg",
        "adult": false,
        "overview": "While searching for her missing mother, intrepid teen Enola Holmes uses her sleuthing skills to outsmart big brother Sherlock and help a runaway lord.",
        "poster_path": "/riYInlsq2kf1AWoGm80JQW5dLKp.jpg",
        "popularity": 2356.996,
        "media_type": "movie"
      },
      {
        "id": 337401,
        "video": false,
        "vote_count": 2257,
        "vote_average": 7.4,
        "title": "Mulan",
        "release_date": "2020-09-04",
        "original_language": "en",
        "original_title": "Mulan",
        "genre_ids": [
          28,
          12,
          18,
          14
        ],
        "backdrop_path": "/zzWGRw277MNoCs3zhyG3YmYQsXv.jpg",
        "adult": false,
        "overview": "When the Emperor of China issues a decree that one man per family must serve in the Imperial Chinese Army to defend the country from Huns, Hua Mulan, the eldest daughter of an honored warrior, steps in to take the place of her ailing father. She is spirited, determined and quick on her feet. Disguised as a man by the name of Hua Jun, she is tested every step of the way and must harness her innermost strength and embrace her true potential.",
        "poster_path": "/aKx1ARwG55zZ0GpRvU2WrGrCG9o.jpg",
        "popularity": 1395.955,
        "media_type": "movie"
      },
      {
        "original_name": "The Boys",
        "id": 76479,
        "name": "The Boys",
        "vote_count": 2532,
        "vote_average": 8.4,
        "first_air_date": "2019-07-25",
        "poster_path": "/mY7SeH4HFFxW1hiI6cWuwCRKptN.jpg",
        "genre_ids": [
          10759,
          10765
        ],
        "original_language": "en",
        "backdrop_path": "/bI37vIHSH7o4IVkq37P8cfxQGMx.jpg",
        "overview": "A group of vigilantes known informally as “The Boys” set out to take down corrupt superheroes with no more than blue-collar grit and a willingness to fight dirty.",
        "origin_country": [
          "US"
        ],
        "popularity": 1819.085,
        "media_type": "tv"
      },
      {
        "original_name": "Raised by Wolves",
        "id": 85723,
        "name": "Raised by Wolves",
        "vote_count": 311,
        "vote_average": 7.6,
        "first_air_date": "2020-09-03",
        "poster_path": "/mTvSVKMn2Npf6zvYNbGMJnYLtvp.jpg",
        "genre_ids": [
          18,
          10765
        ],
        "original_language": "en",
        "backdrop_path": "/na2xUduK8HviOFT97TiFG2MkJmY.jpg",
        "overview": "After Earth is ravaged by a great religious war, an atheistic android architect sends two of his creations, Mother and Father, to start a peaceful, godless colony on the planet Kepler-22b. Their treacherous task is jeopardized by the arrival of the Mithraic, a deeply devout religious order of surviving humans.",
        "origin_country": [
          "US"
        ],
        "popularity": 437.195,
        "media_type": "tv"
      },
      {
        "id": 499932,
        "video": false,
        "vote_count": 888,
        "vote_average": 7.4,
        "title": "The Devil All the Time",
        "release_date": "2020-09-11",
        "original_language": "en",
        "original_title": "The Devil All the Time",
        "genre_ids": [
          80,
          18,
          53
        ],
        "backdrop_path": "/rUeqBuNDR9zN6vZV9kpEFMtQm0E.jpg",
        "adult": false,
        "overview": "In Knockemstiff, Ohio and its neighboring backwoods, sinister characters converge around young Arvin Russell as he fights the evil forces that threaten him and his family.",
        "poster_path": "/7G2VvG1lU8q758uOqU6z2Ds0qpA.jpg",
        "popularity": 286.694,
        "media_type": "movie"
      },
      {
        "id": 621870,
        "video": false,
        "vote_count": 100,
        "vote_average": 7.2,
        "title": "Secret Society of Second Born Royals",
        "release_date": "2020-09-25",
        "original_language": "en",
        "original_title": "Secret Society of Second Born Royals",
        "genre_ids": [
          28,
          12,
          35,
          14
        ],
        "backdrop_path": "/uN4BEmphubHVBSFibqiOwi7wq28.jpg",
        "adult": false,
        "overview": "Sam is a teenage royal rebel, second in line to the throne of the kingdom of Illyria. Just as her disinterest in the royal way of life is at an all-time high, she discovers she has super-human abilities and is invited to join a secret society of similar extraordinary second-born royals charged with keeping the world safe.",
        "poster_path": "/x0fojycYFbT0eqXXbEO6aDqkalX.jpg",
        "popularity": 1258.996,
        "media_type": "movie"
      },
      {
        "original_name": "The Walking Dead",
        "id": 1402,
        "name": "The Walking Dead",
        "vote_count": 7854,
        "vote_average": 7.8,
        "first_air_date": "2010-10-31",
        "poster_path": "/qgjP2OrrX9gc6M270xdPnEmE9tC.jpg",
        "genre_ids": [
          18,
          10759,
          10765
        ],
        "original_language": "en",
        "backdrop_path": "/wXXaPMgrv96NkH8KD1TMdS2d7iq.jpg",
        "overview": "Sheriff's deputy Rick Grimes awakens from a coma to find a post-apocalyptic world dominated by flesh-eating zombies. He sets out to find his family and encounters many other survivors along the way.",
        "origin_country": [
          "US"
        ],
        "popularity": 501.976,
        "media_type": "tv"
      },
      {
        "id": 597156,
        "video": false,
        "vote_count": 53,
        "vote_average": 7,
        "title": "The Boys in the Band",
        "release_date": "2020-09-30",
        "original_language": "en",
        "original_title": "The Boys in the Band",
        "genre_ids": [
          18
        ],
        "backdrop_path": "/wX1n8ndD7l6PsHSUmo66GRJHcwG.jpg",
        "adult": false,
        "overview": "New York, 1968. At a birthday party, a surprise guest and a drunken game leave seven gay friends reckoning with unspoken feelings and buried truths.",
        "poster_path": "/2D2gi13hnrXWaspWuWmdbEZ8bef.jpg",
        "popularity": 280.477,
        "media_type": "movie"
      },
      {
        "original_name": "The Walking Dead: World Beyond",
        "id": 94305,
        "name": "The Walking Dead: World Beyond",
        "vote_count": 6,
        "vote_average": 6.2,
        "first_air_date": "2020-10-04",
        "poster_path": "/8zu2ESmRrCsZrdG1t2sPqXk9tkF.jpg",
        "genre_ids": [
          18,
          10765
        ],
        "original_language": "en",
        "backdrop_path": "/sAKKB0zJIh2JPW2JhuxpzXtEeOg.jpg",
        "overview": "A heroic group of teens sheltered from the dangers of the post-apocalyptic world leave the safety of the only home they have ever known and embark on a cross-country journey to find the one man who can possibly save the world.",
        "origin_country": [
          "US"
        ],
        "popularity": 177.976,
        "media_type": "tv"
      },
      {
        "id": 567971,
        "video": false,
        "vote_count": 27,
        "vote_average": 6.1,
        "title": "Vampires vs. the Bronx",
        "release_date": "2020-10-02",
        "original_language": "en",
        "original_title": "Vampires vs. the Bronx",
        "genre_ids": [
          35,
          27
        ],
        "backdrop_path": "/fvmMtehtEL50A5DqwCoGpTIuVQB.jpg",
        "adult": false,
        "overview": "Three gutsy kids from a rapidly gentrifying Bronx neighborhood stumble upon a sinister plot to suck all the life from their beloved community.",
        "poster_path": "/6VkE7YZKkolB2gTlVYCKjr92ma1.jpg",
        "popularity": 123.506,
        "media_type": "movie"
      },
      {
        "id": 577922,
        "video": false,
        "vote_count": 1928,
        "vote_average": 7.5,
        "title": "Tenet",
        "release_date": "2020-08-22",
        "original_language": "en",
        "original_title": "Tenet",
        "genre_ids": [
          28,
          878,
          53
        ],
        "backdrop_path": "/wzJRB4MKi3yK138bJyuL9nx47y6.jpg",
        "adult": false,
        "overview": "Armed with only one word - Tenet - and fighting for the survival of the entire world, the Protagonist journeys through a twilight world of international espionage on a mission that will unfold in something beyond real time.",
        "poster_path": "/k68nPLbIST6NP96JmTxmZijEvCA.jpg",
        "popularity": 210.553,
        "media_type": "movie"
      },
      {
        "backdrop_path": "/iW74tZ8y2qobdpt4J9UQ71sw8q7.jpg",
        "first_air_date": "2020-10-02",
        "genre_ids": [
          18,
          35
        ],
        "id": 82596,
        "name": "Emily in Paris",
        "origin_country": [
          "US"
        ],
        "original_language": "en",
        "original_name": "Emily in Paris",
        "overview": "When ambitious Chicago marketing exec Emily unexpectedly lands her dream job in Paris, she embraces a new life as she juggles work, friends and romance.",
        "poster_path": "/kwMqIYOC4U9eK4NZnmmyD8pDEOi.jpg",
        "vote_average": 8,
        "vote_count": 8,
        "popularity": 101.004,
        "media_type": "tv"
      },
      {
        "original_name": "Fargo",
        "id": 60622,
        "name": "Fargo",
        "vote_count": 1340,
        "vote_average": 8.3,
        "first_air_date": "2014-04-15",
        "poster_path": "/9ZIhl17uFlXCNUputSEDHwZYIEJ.jpg",
        "genre_ids": [
          80,
          18
        ],
        "original_language": "en",
        "backdrop_path": "/xcJLhsFGVC4LVvucSqVXks2mnUJ.jpg",
        "overview": "A close-knit anthology series dealing with stories involving malice, violence and murder based in and around Minnesota.",
        "origin_country": [
          "US"
        ],
        "popularity": 68.023,
        "media_type": "tv"
      },
      {
        "original_name": "The 100",
        "id": 48866,
        "name": "The 100",
        "vote_count": 4500,
        "vote_average": 7.7,
        "first_air_date": "2014-03-19",
        "poster_path": "/wcaDIAG1QdXQLRaj4vC1EFdBT2.jpg",
        "genre_ids": [
          18,
          10759,
          10765
        ],
        "original_language": "en",
        "backdrop_path": "/hTExot1sfn7dHZjGrk0Aiwpntxt.jpg",
        "overview": "100 years in the future, when the Earth has been abandoned due to radioactivity, the last surviving humans live on an ark orbiting the planet — but the ark won't last forever. So the repressive regime picks 100 expendable juvenile delinquents to send down to Earth to see if the planet is still habitable.",
        "origin_country": [
          "US"
        ],
        "popularity": 549.9,
        "media_type": "tv"
      },
      {
        "original_name": "Ratched",
        "id": 81354,
        "name": "Ratched",
        "vote_count": 254,
        "vote_average": 7.9,
        "first_air_date": "2020-09-18",
        "poster_path": "/cDNxOIm6K5D2W21QyJWZ95sJzQt.jpg",
        "genre_ids": [
          80,
          18,
          9648
        ],
        "original_language": "en",
        "backdrop_path": "/3kT4KvseaDOz3AbVZv1dIvpxEO3.jpg",
        "overview": "An origins story, beginning in 1947, which follows Ratched's journey and evolution from nurse to full-fledged monster tracking her murderous progression through the mental health care system.",
        "origin_country": [
          "US"
        ],
        "popularity": 308.068,
        "media_type": "tv"
      },
      {
        "original_name": "Lovecraft Country",
        "id": 82816,
        "name": "Lovecraft Country",
        "vote_count": 192,
        "vote_average": 7.3,
        "first_air_date": "2020-08-16",
        "poster_path": "/fz7bdjxPColvEWCGr5Kiclzc86d.jpg",
        "genre_ids": [
          18,
          9648,
          10765
        ],
        "original_language": "en",
        "backdrop_path": "/qx7qy2GJOc7yGY6WENyBU3OVv7A.jpg",
        "overview": "The anthology horror series follows 25-year-old Atticus Freeman, who joins up with his friend Letitia and his Uncle George to embark on a road trip across 1950s Jim Crow America to find his missing father. They must survive and overcome both the racist terrors of white America and the malevolent spirits that could be ripped from a Lovecraft paperback.",
        "origin_country": [
          "US"
        ],
        "popularity": 167.389,
        "media_type": "tv"
      },
      {
        "id": 299534,
        "video": false,
        "vote_count": 15215,
        "vote_average": 8.3,
        "title": "Avengers: Endgame",
        "release_date": "2019-04-24",
        "original_language": "en",
        "original_title": "Avengers: Endgame",
        "genre_ids": [
          28,
          12,
          878
        ],
        "backdrop_path": "/orjiB3oUIsyz60hoEqkiGpy5CeO.jpg",
        "adult": false,
        "overview": "After the devastating events of Avengers: Infinity War, the universe is in ruins due to the efforts of the Mad Titan, Thanos. With the help of remaining allies, the Avengers must assemble once more in order to undo Thanos' actions and restore order to the universe once and for all, no matter what consequences may be in store.",
        "poster_path": "/or06FN3Dka5tukK1e9sl16pB3iy.jpg",
        "popularity": 226.829,
        "media_type": "movie"
      },
      {
        "id": 627290,
        "video": false,
        "vote_count": 148,
        "vote_average": 6.1,
        "title": "Antebellum",
        "release_date": "2020-09-02",
        "original_language": "en",
        "original_title": "Antebellum",
        "genre_ids": [
          27
        ],
        "backdrop_path": "/pGqBDYycGWsMYc57sYv5M9GAQoW.jpg",
        "adult": false,
        "overview": "Successful author Veronica finds herself trapped in a horrifying reality and must uncover the mind-bending mystery before it's too late.",
        "poster_path": "/irkse1FMm9dWemwlxKJ7RINT9Iy.jpg",
        "popularity": 594.731,
        "media_type": "movie"
      },
      {
        "id": 741998,
        "video": false,
        "vote_count": 31,
        "vote_average": 5.3,
        "title": "The Binding",
        "release_date": "2020-10-02",
        "original_language": "it",
        "original_title": "Il legame",
        "genre_ids": [
          18,
          27,
          53
        ],
        "backdrop_path": "/tOOfXpIKyY80kY108pwSgLozgRA.jpg",
        "adult": false,
        "overview": "While visiting her fiancé's mother in southern Italy, a woman must fight the mysterious and malevolent curse intent on claiming her daughter.",
        "poster_path": "/k8Oj2YlFKbwj1SNKZcb5Fw1Yosj.jpg",
        "popularity": 73.569,
        "media_type": "movie"
      },
      {
        "id": 501979,
        "video": false,
        "vote_count": 209,
        "vote_average": 6.4,
        "title": "Bill & Ted Face the Music",
        "release_date": "2020-08-27",
        "original_language": "en",
        "original_title": "Bill & Ted Face the Music",
        "genre_ids": [
          12,
          35,
          878
        ],
        "backdrop_path": "/oazPqs1z78LcIOFslbKtJLGlueo.jpg",
        "adult": false,
        "overview": "Yet to fulfill their rock and roll destiny, the now middle-aged best friends Bill and Ted set out on a new adventure when a visitor from the future warns them that only their song can save life as we know it. Along the way, they are helped by their daughters, a new batch of historical figures and a few music legends—to seek the song that will set their world right and bring harmony to the universe.",
        "poster_path": "/4V2nTPfeB59TcqJcUfQ9ziTi7VN.jpg",
        "popularity": 214.408,
        "media_type": "movie"
      }
    ],
    "total_pages": 1000,
    "total_results": 20000
  };