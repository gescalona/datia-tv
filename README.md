# Gregorio Escalona Datia Tv Test

## Create the environment file configuration (.env)

NEXT_PUBLIC_API_URL=https://api.themoviedb.org/3/tv/
NEXT_PUBLIC_API_KEY= ###################### (This value you can take it from themoviedb.org)
NEXT_PUBLIC_LANGUAJE=en-US


## To execute the project.

Run this command to install all requirements.
```bash
yarn
```


To run the Dev version run, the project is available in http://localhost:3000
```bash
 yarn  dev
```