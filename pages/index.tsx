import { SEO } from '@datiatv/components/seo';
import TvShowList  from '@datiatv/components/tv_shows/tv_shows_list';
// import { useSearchTvShow } from '@datiatv/hooks/useSearchTvShow';
//import { TEST_DATA } from '@datiatv/api'

const IndexPage = () => {
 return (
    <>
      <SEO
        title="Datia.tv"
        description="Datia Tv, web application that allows the user to search for his/hers
        favorite TV shows"
      />
      <TvShowList />
    </>
  )
}

export default IndexPage
