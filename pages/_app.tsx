import { AppProps } from 'next/app';
import dynamic from 'next/dynamic';
import {ConfigurationProvider} from '@datiatv/context/app/configurations.provider'
import {AuthProvider} from '@datiatv/context/auth/auth.provider';

import { ThemeProvider } from 'styled-components';

import { defaultTheme } from '@datiatv/site-settings/site-theme/datia';
const AppLayout = dynamic(() => import('../src/components/layout'));

function App({ Component, pageProps }: AppProps) { 

  return (
    <ConfigurationProvider>
      <AuthProvider>
        <ThemeProvider theme={defaultTheme}>
          <style jsx global>{`
            body {
              margin: 0;
              padding: 0;
              font-size: 18px;
              line-height: 1.8;
              font-family: sans-serif;
            }
          `}
          </style>
          <AppLayout>
            <Component {...pageProps} />
          </AppLayout>
        </ThemeProvider>
      </AuthProvider>
    </ConfigurationProvider>
  )
}

export default App