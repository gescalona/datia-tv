import React from 'react';
import { useRouter } from 'next/router';

import TvShowsDetailsComponent from '@datiatv/components/tv_shows/tv_shows_details';

import {useGetTvShowDetail} from '@datiatv/hooks/useGetTvShowDetail';

export default function Details() {
    const { query } = useRouter();
    const [details] = useGetTvShowDetail({ tvShowId: query.id});
    return (
      <TvShowsDetailsComponent tvObj={details} />
    )
}
