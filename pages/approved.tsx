import { SEO } from '@datiatv/components/seo';
import {useRouter} from 'next/router';

const ApprobedPage = () => { 
 const {query} = useRouter();

 console.log(query);
 return (
    <>
      <SEO
        title="Datia.tv - Approbed"
        description="Datia Tv, web application that allows the user to search for his/hers
        favorite TV shows"
      />
      
      <h1>
          Aprobado
      </h1>
    </>
  )
}

export default ApprobedPage
